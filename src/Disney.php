<?php



//Assigment 5 by Wojciech Malecki, Sondre Elstad, Sander Høgli, in cooperation with group 100.
/**
  * Class for accessing DOM data representation of the contents of a Disney.xml
  * file
  */
class Disney
{
    /**
      * The object model holding the content of the XML file.
      * @var DOMDocument
      */
    protected $doc;

    /**
      * An XPath object that simplifies the use of XPath for finding nodes.
      * @var DOMXPath
      */
    protected $xpath;

    /**
      * param String $url The URL of the Disney XML file
      */
    public function __construct($url)
    {
        $this->doc = new DOMDocument();
        $this->doc->load($url);
        $this->xpath = new DOMXPath($this->doc);
    }

    /**
      * Creates an array structure listing all actors and the roles they have
      * played in various movies.
      * returns Array The function returns an array of arrays. The keys of they
      *               "outer" associative array are the names of the actors.
      *                The values are numeric arrays where each array lists
      *                key information about the roles that the actor has
      *                played. The elments of the "inner" arrays are string
      *                formatted this way:
      *               'As <role name> in <movie name> (movie year)' - such as:
      *               array(
      *               "Robert Downey Jr." => array(
      *                  "As Tony Stark in Iron Man (2008)",
      *                  "As Tony Stark in Spider-Man: Homecoming (2017)",
      *                  "As Tony Stark in Avengers: Infinity War (2018)",
      *                  "As Tony Stark in Avengers: Endgame (2019)"),
      *               "Terrence Howard" => array(
      *                  "As Rhodey in Iron Man (2008)")
      *               )
      */
    public function getActorStatistics()
    {
    
    
    
     $actorNodes = $this->xpath->query('//Actor');
     $movieNodes = $this->xpath->query('//Subsidiary/Movie/Cast'); 
     
    
     for($i=0;$i<$actorNodes->length;$i++){
    
         $actorName = $actorNodes->item($i)->childNodes[1]->nodeValue;
         $actorList [$actorName] = array();
         
         
         $actorId = $actorNodes->item($i)->getAttribute('id');
         $roles = $this->xpath->query("//Subsidiary/Movie/Cast/Role[@actor='$actorId']");
         
        
        
            foreach($roles  as $role){
               $rolePath = $role->parentNode->parentNode;
               $roleName = "As ".$role->getAttribute('name')." in ".
               $rolePath->getElementsByTagName('Name')[0]->nodeValue." (".
               $rolePath->getElementsByTagName('Year')[0]->nodeValue.")";
               $actorList[$actorName][] = $roleName;
            }
         }
     
      return $actorList;
    
    }
    public function removeUnreferencedActors()
    {
        $nodes = $this->xpath->query('//Actor');

      for($i=0;$i<$nodes->length;$i++){

         $actorId = $nodes->item($i)->getAttribute("id");
         $role = $this->xpath->query("//Subsidiaries/Subsidiary/Movie/Cast/Role[@actor='$actorId']");
         if ($role->length < 1){
           $nodes->item($i)->parentNode->removeChild($nodes->item($i));
         }
       }

    }

    /**
      * Adds a new role to a movie in the $doc object.
      * @param String $subsidiaryId The id of the Disney subsidiary
      * @param String $movieName    The name of the movie of the new role
      * @param Integer $     movieYear   The production year of the given movie
      * @param String $roleName     The name of the role to be added
      * @param String $roleActor    The id of the actor playing the role
      * @param String $roleAlias    The role's alias (optional)
      */
    public function addRole($subsidiaryId, $movieName, $movieYear, $roleName,
                            $roleActor, $roleAlias = null)
    {
        $role = $this->doc->createElement('Role');
        $movieNode= $this->xpath->query("//Subsidiaries/Subsidiary[@id='$subsidiaryId']/Movie[Name[contains(.,'$movieName')]]/Cast");
        $movieNode->item(0)->appendChild($role);
        $role->setAttribute('name', $roleActor);
        $role->setAttribute('name', $roleAlias );
        
        return "";
    }
}
?>
